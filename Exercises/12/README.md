## Singularity Containers
  
### Task 01: Run an Alpine linux container

Instructions:

1. Submit an interactive job to the training reservation
2. Check the available singularity modules on the cluster
3. Load the latest available `singularityce` module
4. Use Singularity to pull and launch Alpine linux container Using ```singularity shell docker://alpine:latest```
5. Check the OS version on the container by running the command ```cat /etc/os-release``` 