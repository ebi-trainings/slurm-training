## Login to Codon SLURM Cluster

Step 01: SSH to login node
```bash
ssh user@codon-slurm-login.ebi.ac.uk
```

Step 02: Identify hostname
```bash
hostname
```

Step 03: SSH to codon-slurm-login-02 login node
```bash
ssh user@codon-slurm-login-02.ebi.ac.uk
```
