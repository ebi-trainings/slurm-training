## Bonus Exercise

In this exercise we will use Job Arrays and a Array Configuration file.

config.txt lists Array indexes and the other two columns present the task on which this Array will work on.

In slurm.job, when job starts, each Job Array Index will pickup the related task assignment from config.txt file and will print a message.

This is an example exercise to drive motivation on how to use Job Arrays.
