# Codon SLURM Training October 2024

## Training Survey

Please take a minute to fill out training Survey. This helps us in better planning our future trainings.

[Survey Link](https://docs.google.com/forms/d/e/1FAIpQLSehhia1FnhsaXriiTFEygXQBkN9hFFiQvEy83tBtmP7HD7KpA/viewform?usp=sf_link)

## Consultation Slots

If you are interested in having one-on-one consutlation slots with HPC experts, please fill out this form

[Book Consultation Session](https://docs.google.com/spreadsheets/d/1AUQqvV8fNnC9vuL1k1u5O4Ce0kbSxUHYBjrhpKnGiso/edit?usp=drive_link)

### Welcome to Codon SLURM Training session at EBI. This is a 2 Day training course on Codon SLURM Cluster. 

## Useful Links
[Day 01 Slides](https://docs.google.com/presentation/d/15qO34E_tC_j27jftHmY7k5NmThC65HrdDmZJF5Ek-mI/edit?usp=drive_link)

[Day 02 Slides](https://docs.google.com/presentation/d/1nf2B5BYMU_Rd7TLM1w0ZKzEeGebLYPt_OiKzpDmaqkQ/edit?usp=drive_link)

[Exercises](https://gitlab.ebi.ac.uk/ebi-trainings/hpc/slurm-training/-/tree/main/Exercises) 

[Codon Cluster Slack Community](https://ebi.enterprise.slack.com/archives/C042J8B3F33)

[Training Zoom Link](https://embl-org.zoom.us/j/99391931272?pwd=Ug5hSTJ8L01NYDQWFQWvR6uPJNdtoO.1)

## How to Get Started?

This course has multiple hands-on exercises.

Instructors will present a section followed by hands-on exercise.

We recommend to only attempt exercise once the relevant section has been presented.

## Step 00: Join SLACK Channel for this training to ask questions
[Training Slack Channel Link](https://ebi.enterprise.slack.com/archives/C07RGA2UVBL)

## Step 01: Login to Codon SLURM Cluster
```bash
ssh username@codon-slurm-login.ebi.ac.uk
```

## Step 02: Clone Git Repo
Once you have logged into Codon Cluster, you should clone this git repo
```bash
git clone https://gitlab.ebi.ac.uk/ebi-trainings/hpc/slurm-training.git
```

## Step 03: Perform hands-on exercises
Follow instructors slides and do hands-on exercises if any after the each section.
```bash
cd slurm-training
cd Exercises
cd 01
```
